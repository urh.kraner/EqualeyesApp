//
//  Util.swift
//  EqualeyesApp
//
//  Created by Urh on 14/05/2021.
//

import Foundation

func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
    URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
}

func getImageFromUrl(from url: URL, completion: @escaping (Data) -> ()) {
    print("Download Started")
    getData(from: url) { data, response, error in
        guard let data = data, error == nil else {
            print("Unable to download file!")
            return
        }
        print("Download Finished")
        completion(data)
    }
}

