//
//  NetworkManager.swift
//  EqualeyesApp
//
//  Created by Urh on 13/05/2021.
//

import Foundation

let studentUrl = URL(string: "https://zpk2uivb1i.execute-api.us-east-1.amazonaws.com/dev/students")
let teacherUrl = URL(string: "https://zpk2uivb1i.execute-api.us-east-1.amazonaws.com/dev/teachers")

func fetchStudents(completionHandler: @escaping ([Student]) -> Void) {
    
    let task = URLSession.shared.dataTask(with: studentUrl!, completionHandler: { (data, response, error) in
        if let error = error {
            print("Error with fetching students: \(error)")
            return
        }
        
        guard let httpResponse = response as? HTTPURLResponse,
              (200...299).contains(httpResponse.statusCode) else {
            print("Error with the response, unexpected status code: \(String(describing: response))")
            return
        }
        
        if let data = data,
           let students = try? JSONDecoder().decode([Student].self, from: data) {
        
            DispatchQueue.main.async {
                completionHandler(students)
            }
        }
    })
    task.resume()
}

func fetchTeachers(completionHandler: @escaping ([Teacher]) -> Void) {
    
    let task = URLSession.shared.dataTask(with: teacherUrl!, completionHandler: { (data, response, error) in
        if let error = error {
            print("Error with fetching teachers: \(error)")
            return
        }
        
        guard let httpResponse = response as? HTTPURLResponse,
              (200...299).contains(httpResponse.statusCode) else {
            print("Error with the response, unexpected status code: \(String(describing: response))")
            return
        }
        
        if let data = data,
           let teachers = try? JSONDecoder().decode([Teacher].self, from: data) {
            
            DispatchQueue.main.async {
                completionHandler(teachers)
            }
        }
    })
    task.resume()
}
