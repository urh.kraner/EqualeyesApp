//
//  PersonDetailVC.swift
//  EqualeyesApp
//
//  Created by Urh on 14/05/2021.
//

import UIKit

class PersonDetailVC: UIViewController {
    
    @IBOutlet weak var detailViewGroup: UIView!
    @IBOutlet weak var contactButton: UIButton!
    
    @IBOutlet weak var personNameLabel: UILabel!
    @IBOutlet weak var classOrGradeLabel: UILabel!
    @IBOutlet weak var schoolLabel: UILabel!
    
    var person: Person? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpViews()
        if let person = self.person {
            setUpPerson(person: person)
        }
    }
    
    func setUpViews(){
        detailViewGroup.layer.cornerRadius = 8
        contactButton.layer.cornerRadius = 8
    }
    
    func setUpPerson(person: Person) {
        personNameLabel.text = person.name
        
        if let person = person as? Teacher {
            classOrGradeLabel.text = person.schoolClass
            schoolLabel.text = "School: \(person.school_id)"
            schoolLabel.isHidden = false
        } else if let person = person as? Student {
            contactButton.isHidden = true
            classOrGradeLabel.text = "Grade: \(person.grade)"
            schoolLabel.isHidden = true
        }
    }
    
    @IBAction func contactButtonPress (sender: Any){
        UIApplication.shared.keyWindow?.rootViewController?.present(createAlert(), animated: true, completion: {
            
        })
    }
    
    func createAlert() -> UIAlertController {
        var title = "Contact"
        if let name = self.person?.name{
            title = "Contact \(name)"
        }
        
        let alert = UIAlertController(title: title, message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Email", style: .default , handler:{ (UIAlertAction)in
            print("Email clicked")
        }))
        
        alert.addAction(UIAlertAction(title: "Message", style: .default , handler:{ (UIAlertAction)in
            print("Message clicked")
        }))

        alert.addAction(UIAlertAction(title: "Call", style: .default , handler:{ (UIAlertAction)in
            print("Call clicked")
        }))
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            print("Dismiss clicked")
        }))
        
        return alert
    }
    
}
