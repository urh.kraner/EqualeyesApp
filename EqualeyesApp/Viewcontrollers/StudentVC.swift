//
//  StudentVC.swift
//  EqualeyesApp
//
//  Created by Urh on 12/05/2021.
//

import UIKit

class StudentVC: UIViewController {
    
    @IBOutlet weak var studentTableView: UITableView!
    
    let cellSpacingHeight: CGFloat = 5
    var students: [Student] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureView()
        
        fetchStudents { (students) in
            self.students = students
            self.studentTableView.reloadData()
        }
    }
    
    func configureView(){
        self.studentTableView.dataSource = self
        self.studentTableView.delegate = self
        registerTableCells()
    }
    
    func registerTableCells(){
        let studentCell = UINib(nibName: "StudentCell", bundle: nil)
        self.studentTableView.register(studentCell, forCellReuseIdentifier: "StudentCell")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if segue.identifier == "showStudentDetails" ,
           let nextScene = segue.destination as? PersonDetailVC ,
           let indexPath = self.studentTableView.indexPathForSelectedRow {
            let selectedStudent = students[indexPath.section]
            nextScene.person = selectedStudent
        }
    }
    
}

extension StudentVC: UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.students.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return cellSpacingHeight
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = studentTableView.dequeueReusableCell(withIdentifier: "StudentCell") as? StudentCell {
            cell.setStudent(student: self.students[indexPath.section])
            
            cell.layer.cornerRadius = 8
            cell.clipsToBounds = true
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "showStudentDetails", sender: self)
    }
}
