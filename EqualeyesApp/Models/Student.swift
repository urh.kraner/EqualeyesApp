//
//  Student.swift
//  EqualeyesApp
//
//  Created by Urh on 13/05/2021.
//

import Foundation

class Student: Person {
    var grade : Int
    
    private enum CodingKeys: String, CodingKey {
        case grade = "grade"
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        grade = try container.decode(Int.self, forKey: .grade)
        try super.init(from: decoder)
    }

}
