//
//  Person.swift
//  EqualeyesApp
//
//  Created by Urh on 13/05/2021.
//

import Foundation

class Person : Decodable{
    var name : String
    var school_id : Int
    
    private enum CodingKeys: String, CodingKey {
        case name = "name"
        case schoolId = "school_id"
    }
    
    init(name : String, schoolId : Int ) {
        self.name = name
        self.school_id = schoolId
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        name = try container.decode(String.self, forKey: .name)
        school_id = try container.decode(Int.self, forKey: .schoolId)
    }
}
