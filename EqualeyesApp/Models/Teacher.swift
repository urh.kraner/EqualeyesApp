//
//  Teacher.swift
//  EqualeyesApp
//
//  Created by Urh on 13/05/2021.
//

import Foundation

class Teacher: Person{
    //var name: String
    //var school_id: Int
    var schoolClass: String
    var image_url : String
    
    
    private enum CodingKeys: String, CodingKey {
        case schoolClass = "class"
        case imageUrl = "image_url"
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        schoolClass = try container.decode(String.self, forKey: .schoolClass)
        image_url = try container.decode(String.self, forKey: .imageUrl)
        try super.init(from: decoder)
    }
}
