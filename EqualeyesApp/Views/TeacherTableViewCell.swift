//
//  TeacherTableViewCell.swift
//  EqualeyesApp
//
//  Created by Urh on 14/05/2021.
//

import UIKit

class TeacherTableViewCell: UITableViewCell {
    
    private var teacher: Teacher? = nil
    
    @IBOutlet weak var teacherImage: UIImageView!
    @IBOutlet weak var classLabel: UILabel!
    @IBOutlet weak var teacherNameLabel: UILabel!
    @IBOutlet weak var contactButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.contactButton.layer.cornerRadius = 8
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setTeacher(teacher: Teacher){
        self.teacher = teacher
        teacherNameLabel.text = teacher.name
        classLabel.text = teacher.schoolClass
        guard let imageUrl: URL = URL(string: teacher.image_url) else { return }
        getImageFromUrl(from: imageUrl) { image in
            DispatchQueue.main.async {
                self.teacherImage.image = UIImage(data: image)
            }
        }
    }
    
    @IBAction func contactButtonPress (sender: Any){
        print("Contact pressed")
        UIApplication.shared.keyWindow?.rootViewController?.present(createAlert(), animated: true, completion: {
            
        })
    }
    
    func createAlert() -> UIAlertController {
        var title = "Contact"
        if let name = self.teacher?.name{
            title = "Contact \(name)"
        }
        
        let alert = UIAlertController(title: title, message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Email", style: .default , handler:{ (UIAlertAction)in
            print("Email clicked")
        }))
        
        alert.addAction(UIAlertAction(title: "Message", style: .default , handler:{ (UIAlertAction)in
            print("Message clicked")
        }))

        alert.addAction(UIAlertAction(title: "Call", style: .default , handler:{ (UIAlertAction)in
            print("Call clicked")
        }))
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            print("Dismiss clicked")
        }))
        
        return alert
    }
    
}
