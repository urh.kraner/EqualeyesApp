//
//  StudentCell.swift
//  EqualeyesApp
//
//  Created by Urh on 13/05/2021.
//

import UIKit

class StudentCell: UITableViewCell {

    @IBOutlet weak var studentNameLabel: UILabel!
    @IBOutlet weak var gradeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = 8
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func setStudent(student: Student){
        self.studentNameLabel.text = student.name
        self.gradeLabel.text = "Grade: \(student.grade)"
    }
    
}
