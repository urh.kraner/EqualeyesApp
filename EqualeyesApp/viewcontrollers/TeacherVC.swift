//
//  TeacherVC.swift
//  EqualeyesApp
//
//  Created by Urh on 13/05/2021.
//

import UIKit

class TeacherVC: UIViewController {
    
    @IBOutlet weak var teacherTableView: UITableView!
    
    let cellSpacingHeight: CGFloat = 5
    var teachers: [Teacher] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureTableView()
        
        fetchTeachers { (teachers) in
            self.teachers = teachers
            self.teacherTableView.reloadData()
        }        
    }
    
    func configureTableView() {
        setNavigationTitle()
        
        teacherTableView.dataSource = self
        teacherTableView.delegate = self
        registerTableViewCells()
    }
    
    private func setNavigationTitle() {
        if let title =
            Bundle.main.object(forInfoDictionaryKey: "TeachersTitle") as? String
        {
            self.title = title
        }
    }
    
    
    private func registerTableViewCells() {
        let teacherCell = UINib(nibName: "TeacherTableViewCell",
                                bundle: nil)
        teacherTableView.register(teacherCell,
                                  forCellReuseIdentifier: "TeacherTableViewCell")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if segue.identifier == "showTeacherDetails" ,
           let nextScene = segue.destination as? PersonDetailVC ,
           let indexPath =     self.teacherTableView.indexPathForSelectedRow {
            let selectedTeacher = teachers[indexPath.section]
            nextScene.person = selectedTeacher
        }
    }
    
}

extension TeacherVC:  UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.teachers.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return cellSpacingHeight
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = teacherTableView.dequeueReusableCell(withIdentifier: "TeacherTableViewCell") as? TeacherTableViewCell {
            cell.setTeacher(teacher: self.teachers[indexPath.section])
            
            cell.layer.cornerRadius = 8
            cell.clipsToBounds = true
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "showTeacherDetails", sender: self)
    }
}
